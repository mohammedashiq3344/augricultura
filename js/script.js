function resize(){

}

$(document).ready(function(){

	$('header span.menu-icon').click(function(){
		$('header nav.mobile-menu').slideDown(function(){
			$('header span.close-icon').show()
		})
	});

	$('header span.close-icon').click(function(){
		$('header nav.mobile-menu').slideUp(function(){
			$('header span.close-icon').hide()
		})
	});
	var owl = $('.owl-carousel');
	owl.owlCarousel({
    loop:true,
    margin:10,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        }
        
    }
	});
	owl.on('mousewheel', '.owl-stage', function (e) {
		if (e.deltaY>0) {
			owl.trigger('next.owl');
		} else {
			owl.trigger('prev.owl');
		}
		e.preventDefault();
	});
	AOS.init({
		duration: 800,
		easing: 'slide',
		once: false
	});

})
$(window).resize(function(){

    resize();
});

$(window).on('load',function(){
    resize();
});

